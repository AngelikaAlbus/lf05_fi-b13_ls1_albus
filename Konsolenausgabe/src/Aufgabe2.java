
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Einen neuen Absatz beginnt man mit \n am Ende des geschriebenen Satzes!
		
				System.out.print("\n");
				
				//Ausgabe: Tannenbaum
				
				System.out.printf("         *\n");
				System.out.printf("        ***\n");
				System.out.printf("       *****\n");
				System.out.printf("      *******\n");
				System.out.printf("     *********\n");
				System.out.printf("    ***********\n");
				System.out.printf("   *************\n");
				System.out.printf("         ***\n"); 
				System.out.printf("         ***\n");
	}

}


public class aufgabe3 {

	public static void main(String[] args) {
		// 1. String erstellen, 2.int angeben, 
		//3. double benennen - .2f f�r 2 Stellen nach dem Komma
		//4. Die 2. Zeile ist die Trennlinie f�r die Tabelle
		//5. %-s linksb�ndig | %s rechtsb�ndig - die Stellenbreite wird vor "s" angegeben
		

		String f = "Fahrenheit";
		String c = "Celsius";
		int zeile1= -20;
		int	zeile2=-10;
		int zeile3=+0;
		int zeile4=+20;
		int zeile5=+30;
		double z1=-28.8889;
		double z2=-23.3333;
		double z3=-17.7778;
		double z4=-6.6667;
		double z5=-1.1111;
		
		System.out.printf("%-12s|%10s\n", f, c);
		System.out.printf("-----------------------\n");
		System.out.printf("%-12s|%10.2f\n",zeile1,z1);
		System.out.printf("%-12s|%10.2f\n",zeile2,z2);
		System.out.printf("+%-11s|%10.2f\n",zeile3,z3);
		System.out.printf("+%-11s|%10.2f\n",zeile4,z4);
		System.out.printf("+%-11s|%10.2f\n",zeile5,z5);
		

	}

}

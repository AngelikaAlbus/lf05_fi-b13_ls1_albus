import java.util.Scanner;

public class aufgabe8_Schaltjahr {

	public static void main(String[] args) {
		// TODO Regel 1 ganzzahlige Jahreszahl durch 4 teilbar, ist ein Schaltjahr mit
		// 366 Tagen
		// Regel 2 Ausnahme von Regel 1: Jahreszahlen, die durch 100 teilbar sind, sind
		// keine Schaltjahre
		// Regel 3 Ausnahme von Regel 2: Wenn die Jahreszahl kein Schaltjahr ist, sie
		// aber durch 400 teilbar sind, dann
		// dann gelten diese jahre mit 366 Tagen

		// Eingabe
		System.out.println("Willkommen bei der Berechnung von Schaltjahren!");
		Scanner scan = new Scanner(System.in); // Scanner wird angelegt
		
		// Verarbeitung
		System.out.println("Gebe die gewŁnschte Jahreszahl ein!:");
		int jahr = scan.nextInt();
		

		// Ausgabe

		System.out.println("Das genannte Jahr " + jahr + " ist ein Schaltjahr! ");

	}

	public boolean istSchaltjahr(int jahr) {
		if(jahr %4 == 0 ) {
		   return true;
		}
		else if (jahr %100 == 0) {
		   return true;
		}
		else if (jahr %400 == 0) {
		   return true;
		}
		else {
			return false;
		}
	}
	
}

	
import java.util.Scanner;

public class aufgabe5_OhmschesGesetz {

	public static void main(String[] args) {
		// TODO Eingabe: Aufforderung der zu berechnenden Werte
		
		double u_spannungVolt;
		double i_stromstaerkeAmpere;
		double r_widerstandOhm;
		char c;
		
		Scanner scan = new Scanner(System.in);
		
		//Eingabeaufforderung
		
		System.out.println("Welchen Wert wollen Sie ermitteln? Eingabe von 1 = U für Spannung, 2 = I für Stromstearke, 3 = R für Widerstand: ");
		c = scan.next().charAt(0);
		
		
		//Verarbeitung und Ausgabe: Berechnung des gewaehlten Falls 1,2 oder 3
		
		switch (c) {
		
		case '1':
			System.out.println("Geben Sie den Wert für Widerstand (Ω) ein!: ");
			r_widerstandOhm = scan.nextDouble();
			System.out.println("Geben Sie den Wert von Stromstärke (I) ein!: ");
			i_stromstaerkeAmpere = scan.nextDouble();
			
			u_spannungVolt = r_widerstandOhm * i_stromstaerkeAmpere;						// Formel U= R*I
			System.out.println("Der Spannungswert lautet:" + u_spannungVolt + "V");
				break;
		
		case '2':
			System.out.println("Geben Sie den Wert für Spannung (U) ein!: ");
			u_spannungVolt = scan.nextDouble();
			System.out.println("Geben Sie den Wert für Widerstand (R) ein!: ");
			r_widerstandOhm = scan.nextDouble();
			
			i_stromstaerkeAmpere = u_spannungVolt / r_widerstandOhm;						// Formel I = U/R
			System.out.println("Der Spannungswert lautet:" + i_stromstaerkeAmpere + "A");
				break;
				
		case '3':
			System.out.println("Geben Sie den Wert für Spannung (U) ein!: ");
			u_spannungVolt = scan.nextDouble();
			System.out.println("Geben Sie den Wert für Stromstärke (I) ein!: ");
			i_stromstaerkeAmpere = scan.nextDouble();
			
			r_widerstandOhm = u_spannungVolt / i_stromstaerkeAmpere;						// Formel R= U/I
			System.out.println("Der Widerstandswert lautet:" + r_widerstandOhm + "Ω");
				break;
				
		default:
			System.out.println("Bitte entscheiden Sie sich nur für 1, 2 oder 3! ");
		}
		
	}

}

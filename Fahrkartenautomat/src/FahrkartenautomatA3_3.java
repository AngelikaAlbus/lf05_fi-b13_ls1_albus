
import java.util.Scanner;

class FahrkartenautomatA3_3{
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
  
       double zuZahlen = fahrkartenbestellungErfassung();
       double rueckgabebetrag = fahrkartenBezahlen(zuZahlen);
       fahrkartenAusgeben(); 
       rueckgeldAusgeben(rueckgabebetrag);
             
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
       /**TODO Erstellen Sie f�r einzelnen Ablaufphasen des Fahrkartenautomaten
        die vier Methoden
       - Fahrkarten werden bestellt -> fahrbestellungErfassung = zuZahlenderBetrag wird angegeben
       - Geld muss bezahlt werden -> fahrkartenBezahlen = eingezahlterBetrag - zuZahlen
       - Fahrkarten werden ausgedruckt -> fahrkartenAusgeben = Druck der Karten
       - Geldeinwurf wird erfasst -> rueckgeldAusgabe = rueckgabebetrag /Geldaussch�ttung */
    
       public static double fahrkartenbestellungErfassung()
       {
    	   Scanner tastatur = new Scanner(System.in);
    	   System.out.print("Ticketpreis (EURO): ");
           double einzelpreis = tastatur.nextDouble();

           //  Eingabe Anzahl der Tickets
           System.out.print("Anzahl der Tickets: ");
           int ticketanzahl = tastatur.nextInt();
           
           // Berechnung zu zahlender Betrag 
           System.out.print("Zu zahlender Betrag (EURO): ");
           double zuZahlenderBetrag = ticketanzahl* einzelpreis;
           
		return zuZahlenderBetrag;
       }
       
       public static double fahrkartenBezahlen(double zuZahlen)
    	   {
    	   double eingezahlterGesamtbetrag;
           double eingeworfeneMuenze;
    	   Scanner tastatur = new Scanner(System.in);
        // Geldeinwurf
           // -----------
           eingezahlterGesamtbetrag = 0.0;
           while(eingezahlterGesamtbetrag < zuZahlen)
           {
        	   System.out.printf("Noch zu zahlen: "+ "%.2f" , (zuZahlen - eingezahlterGesamtbetrag));
        	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
        	   
        	   eingeworfeneMuenze = tastatur.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneMuenze;
           }
    		   return (eingezahlterGesamtbetrag-zuZahlen);
    	   }
       public static void fahrkartenAusgeben()
       {
    	   // Fahrscheinausgabe
           // -----------------
           System.out.println("\nFahrschein wird ausgegeben");
           for (int i = 0; i < 8; i++)
           {
              System.out.print("=");
              try {
    			Thread.sleep(250);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
           }
           System.out.println("\n\n");
       }
       public static void rueckgeldAusgeben(double rueckgabebetrag)
       {
    	   // R�ckgeldberechnung und -Ausgabe
           // -------------------------------
    	   String s = String.valueOf(rueckgabebetrag);
		       if(rueckgabebetrag > 0.0)
           {
		    	   
        	   System.out.print("Der R�ckgabebetrag in H�he von " + s + " EURO" +
        	   "\n wird in folgenden M�nzen ausgezahlt:");

               while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
               {
            	  System.out.println("2 EURO");
    	          rueckgabebetrag -= 2.0;
               }
               while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
               {
            	  System.out.println("1 EURO");
    	          rueckgabebetrag -= 1.0;
               }
               while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
               {
            	  System.out.println("50 CENT");
    	          rueckgabebetrag -= 0.5;
               }
               while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
               {
            	  System.out.println("20 CENT");
     	          rueckgabebetrag -= 0.2;
               }
               while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
               {
            	  System.out.println("10 CENT");
    	          rueckgabebetrag -= 0.1;
               }
               while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
               {
            	  System.out.println("5 CENT");
     	          rueckgabebetrag -= 0.05;
       }
      }
     }
}





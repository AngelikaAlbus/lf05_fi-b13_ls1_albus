import java.util.Scanner;

class FahrkartenautomatA2_5
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       int ticketanzahl;
       double einzelpreis;
       
//       Eingabe des Ticketpreises
       System.out.print("Ticketpreis (EURO): ");
       einzelpreis = tastatur.nextDouble();

//       Eingabe Anzahl der Tickets
       System.out.print("Anzahl der Tickets: ");
       ticketanzahl = tastatur.nextInt();
       
//       Berechnung zu zahlender Betrag 
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = ticketanzahl* einzelpreis;
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: "+ "%.2f" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0);
       {
    	   System.out.println("Der R�ckgabebetrag in H�he von %4.2f � %n " + r�ckgabebetrag + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
}
// Punkt 5 - Wahl des Datentypes: Int f�r Ticketanzahl, eine Ganzzahl, byte w�re evtl. zu kurz. Man k�nnte theoretisch auch short nehmen
// (32767 m�gliche Tickekts). Long w�re �bertrieben da mehr als 2147483647 Tickets m�glich w�ren.

// Punkt 6 - Erl�uterung der Berechnung: 
// Die Variable "zuZahlenderBetrag" wird durch den Arithmetischen Operator "Produkt" der Variablen "Ticketanzahl" und "Einzelpreis", deklariert.
// Das Ergebnis wird als Double ausgegeben, da die Multiplikation eines Integer mit einem Double, weiterhin eine Gleitkommazahl bleiben kann
//


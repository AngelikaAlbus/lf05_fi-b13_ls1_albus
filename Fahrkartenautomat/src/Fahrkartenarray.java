import java.util.Scanner;

public class Fahrkartenarray {

	public static void main(String[] args) throws Exception {
		
		// TODO Automatisch generierter Methodenstub
			Scanner tastatur = new Scanner(System.in);

				double zuZahlen = fahrkartenBestellungErfassung();
				double rueckgabebetrag = fahrkartenBezahlen(zuZahlen);
				fahrkartenAusgeben(); 
				rueckgeldAusgeben(rueckgabebetrag);
	      
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir wünschen Ihnen eine gute Fahrt.");

	}
				/**TODO 
				 */
	
			public static double fahrkartenBestellungErfassung() throws Exception
		
			{
				Scanner tastatur = new Scanner(System.in);
				String[] fahrkartenAuflistung= new String[10];
				 double[] preise = new double[10];
				  
				 fahrkartenAuflistung[0] = "Einzelfahrschein Berlin AB ";						//Aufstellung der Fahrkartenarten
				 fahrkartenAuflistung[1] = "Einzelfahrschein Berlin BC ";
				 fahrkartenAuflistung[2] = "Einzelfahrschein Berlin ABC ";	
				 fahrkartenAuflistung[3] = "Kurzstrecke ";
				 fahrkartenAuflistung[4] = "Tageskarte Berlin AB";
				 fahrkartenAuflistung[5] = "Tageskarte Berlin BC";
				 fahrkartenAuflistung[6] = "Tageskarte Berlin ABC";
				 fahrkartenAuflistung[7] = "Kleingruppen-Tageskarte Berlin AB";
				 fahrkartenAuflistung[8] = "Kleingruppen-Tageskarte Berlin BC";
				 fahrkartenAuflistung[9] = "Kleingruppen-Tageskarte Berlin ABC";
				 
				 preise[0] = 2.90; 																//Aufstellung der dazugehörende Preise im Index
				 preise[1] = 3.30;
				 preise[2] = 3.60;
				 preise[3] = 1.90;
				 preise[4] = 8.60;
				 preise[5] = 9.00;
				 preise[6] = 9.60;
				 preise[7] = 23.50;
				 preise[8] = 24.30;
				 preise[9] = 24.90;
				 
				/**TODO 
				*/
				
				 System.out.println ("Waehlen Sie Ihr Ticket!");
			 for(int i=0; i< fahrkartenAuflistung.length; i++) {
					 System.out.println(fahrkartenAuflistung[i]);
			 }
				 int ticket = tastatur.nextInt();
				 
				 while (ticket < 0 || ticket > fahrkartenAuflistung.length) {				//Schleife, die waehrend der Ticketeingabe die Laenge des Arrays ueberprueft 		
					 if (ticket > 0 && ticket <= fahrkartenAuflistung.length) {				//wenn Ticket grösser und Arraylaenge kleiner|gleich ist
						 ticket = ticket -1;												// Arrayabfrage wird um 1 minimiert um im Index zu sein
						 break;
					 }
					 System.out.println("Falsche Eingabe. Ticket existiert nicht! Erneut Versuchen");			
					 ticket = tastatur.nextInt();
				 }
					
				   System.out.print("Ticketpreis (EURO): ");
			     double einzelpreis = tastatur.nextDouble();
			
			     //  Eingabe Anzahl der Tickets
			     System.out.print("Anzahl der Tickets: ");
			     int ticketanzahl = tastatur.nextInt();
			    
			     // Berechnung zu zahlender Betrag 
			     System.out.print("Zu zahlender Betrag (EURO): ");
			     double zuZahlenderBetrag = ticketanzahl* einzelpreis;
			    
				 return zuZahlenderBetrag;
			    }
			
			public static double fahrkartenBezahlen(double zuZahlen)
				   {
				   double eingezahlterGesamtbetrag;
			    double eingeworfeneMuenze;
				   Scanner tastatur = new Scanner(System.in);
				   
			    // Geldeinwurf
			    // -----------
				   
				   
			    eingezahlterGesamtbetrag = 0.0;
			    while(eingezahlterGesamtbetrag < zuZahlen)
			    {
			 	   System.out.printf("Noch zu zahlen: "+ "%.2f" , (zuZahlen - eingezahlterGesamtbetrag));
			 	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
			 	   
			 	   eingeworfeneMuenze = tastatur.nextDouble();
			        eingezahlterGesamtbetrag += eingeworfeneMuenze;
			    }
					   return (eingezahlterGesamtbetrag-zuZahlen);
				   }
			public static void fahrkartenAusgeben()
			{
			   // Fahrscheinausgabe
			    // -----------------
			    System.out.println("\nFahrschein wird ausgegeben");
			    for (int i = 0; i < 8; i++)
			    {
			       System.out.print("=");
			       try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			    System.out.println("\n\n");
			}
			public static void rueckgeldAusgeben(double rueckgabebetrag)
			{
				   // Rückgeldberechnung und -Ausgabe
			    // -------------------------------
				   String s = String.valueOf(rueckgabebetrag);
				       if(rueckgabebetrag > 0.0)
			    {
				    	   
			 	   System.out.print("Der Rückgabebetrag in Höhe von " + s + " EURO" +
			 	   "\n wird in folgenden Münzen ausgezahlt:");
			
			        while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			        {
			     	  System.out.println("2 EURO");
				          rueckgabebetrag -= 2.0;
			        }
			        while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			        {
			     	  System.out.println("1 EURO");
				          rueckgabebetrag -= 1.0;
			        }
			        while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			        {
			     	  System.out.println("50 CENT");
				          rueckgabebetrag -= 0.5;
			        }
			        while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			        {
			     	  System.out.println("20 CENT");
				          rueckgabebetrag -= 0.2;
			        }
			        while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			        {
			     	  System.out.println("10 CENT");
				          rueckgabebetrag -= 0.1;
			        }
			        while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			        {
			     	  System.out.println("5 CENT");
				          rueckgabebetrag -= 0.05;
				          
			}
		}	
	}
}
